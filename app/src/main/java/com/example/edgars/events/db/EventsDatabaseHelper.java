package com.example.edgars.events.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Edgars on 08-Mar-16.
 */
public class EventsDatabaseHelper extends SQLiteOpenHelper {

    public final String LOG = EventsDatabaseHelper.class.getSimpleName();

    private static EventsDatabaseHelper dbHelper = null;
    public static long rowId;

    /**
     * Singleton for only one opened connection.
     *
     * @param context Application context.
     * @return EventsDatabaseHelper object for DB operations.
     */
    public static EventsDatabaseHelper getInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new EventsDatabaseHelper(context.getApplicationContext());
        }
        return dbHelper;
    }

    public static final String DATABASE_NAME = "events.db";
    private static final int DATABASE_VERSION = 1;
    public static final String _ID = "id";

    /**
     * Database columns.
     * Events.
     */
    public static final String EVENT_TABLE = "event";
    public static final String EVENT_NAME = "event_name";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String EVENT_IMAGE = "event_image";
    public static final String PRICE = "price";
    public static final String EVENT_OTHER_DETAILS = "event_other_details";

    /**
     * Database columns.
     * Venue.
     */
    public static final String VENUE_TABLE = "venue";
    public static final String VENUE_NAME = "venue_name";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String STREET_NUMBER = "street_number";
    public static final String CITY = "city";
    public static final String ADDRESS = "address";
    public static final String COUNTRY = "country";
    public static final String VENUE_IMAGE = "venue_image";
    public static final String CAPABILITY = "capability";
    public static final String VENUE_OTHER_DETAILS = "venue_other_details";
    public static final String EVENT_ID = "event_id";

    /**
     * Create Event table.
     */
    private final String CREATE_EVENT_TABLE = "CREATE TABLE IF NOT EXISTS " + EVENT_TABLE + " (" +
            _ID + " INTEGER PRIMARY KEY, " +
            EVENT_NAME + " TEXT NOT NULL, " +
            START_DATE + " TEXT NOT NULL, " +
            END_DATE + " TEXT, " +
            EVENT_IMAGE + " TEXT NOT NULL, " +
            PRICE + " TEXT, " +
            EVENT_OTHER_DETAILS + " TEXT " +
            ");";

    /**
     * Create Venue table.
     */
    private final String CREATE_VENUE_TABLE = "CREATE TABLE IF NOT EXISTS " + VENUE_TABLE + " (" +
            _ID + " INTEGER PRIMARY KEY, " +
            VENUE_NAME + " TEXT NOT NULL, " +
            LAT + " REAL, " +
            LNG + " REAL, " +
            STREET_NUMBER + " TEXT, " +
            CITY + " TEXT NOT NULL," +
            ADDRESS + " TEXT, " +
            COUNTRY + " TEXT, " +
            VENUE_IMAGE + " TEXT NOT NULL," +
            CAPABILITY + " TEXT," +
            VENUE_OTHER_DETAILS + " TEXT, " +
            EVENT_ID + " TEXT NOT NULL " +
            ");";

    public EventsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // temporary deletion db every time.
        db.execSQL("DROP TABLE IF EXISTS " + EVENT_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + VENUE_TABLE);

        db.execSQL(CREATE_EVENT_TABLE);
        db.execSQL(CREATE_VENUE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + EVENT_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + VENUE_TABLE);
        onCreate(db);
    }

    private boolean checkIfExists(Context context, ContentValues contentValues, String tableName) {
        SQLiteDatabase db = EventsDatabaseHelper.getInstance(context).getReadableDatabase();

        String query = "SELECT " + EVENT_NAME + " FROM " + EVENT_TABLE +
                " WHERE " + EVENT_NAME + " = "  + "'" + contentValues.getAsString(EVENT_NAME) + "'";
        Cursor cursor = db.rawQuery(query, null);

        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        System.out.println("EXISTS: " + exists);
        return exists;
    }

    public void save(Context context, ContentValues contentValues, String tableName) {
        Log.i(LOG, "save()");
        SQLiteDatabase db = EventsDatabaseHelper.getInstance(context).getWritableDatabase();
        if (!checkIfExists(context, contentValues, tableName)) {
            rowId = db.insert(
                    tableName,
                    null,
                    contentValues
            );
        }

    }

    public Cursor read(Context context) {
        SQLiteDatabase db = EventsDatabaseHelper.getInstance(context).getReadableDatabase();

        String[] projection = {
                EVENT_NAME,
                EVENT_IMAGE,
                START_DATE
        };

        String sortOrder = START_DATE + " DESC";

        Cursor cursor = db.query(
                EVENT_TABLE,
                projection,
                null,
                null,
                null,
                null,
                sortOrder
        );

        return cursor;
    }

}
