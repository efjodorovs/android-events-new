package com.example.edgars.events;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;

/**
 * Activity creates a EventListFragment.
 * App will start from the list of Events.
 *
 * Created by Edgars on 03-Mar-16.
 */
public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new EventListFragment()).commit();
        }

    }
}
