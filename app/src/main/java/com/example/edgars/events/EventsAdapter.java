package com.example.edgars.events;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class EventsAdapter extends CursorAdapter {

    public static class ViewHolder {
        public final ImageView eventImage;
        public final TextView eventName;
        public final TextView startDate;

        public ViewHolder(View view) {
            eventImage = (ImageView) view.findViewById(R.id.list_item_image);
            eventName = (TextView) view.findViewById(R.id.list_item_title);
            startDate = (TextView) view.findViewById(R.id.list_item_start_date);
        }
    }

    public EventsAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

    }
}
