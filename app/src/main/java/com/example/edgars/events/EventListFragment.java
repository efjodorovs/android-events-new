package com.example.edgars.events;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.edgars.events.db.EventsDatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Fragment that lists out loaded events from website.
 * Created by Edgars on 03-Mar-16.
 */
public class EventListFragment extends Fragment {

    private Context mContext;

    public final String LOG = EventListFragment.class.getSimpleName();

    private EventsAdapter mEventsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mContext = getActivity().getApplicationContext();
        View view = inflater.inflate(R.layout.fragment_event_list, container, false);

        update(1);

        return view;
    }

    private void update(int pageNum) {
        Log.i(LOG, "update()");
        new JsonTask().execute(pageNum);
    }

    private class JsonTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... page) {
            Log.d(LOG, "doInBackground()");

            HttpURLConnection urlConnection = null;
            BufferedReader bufferedReader = null;

            String eventString = null;
            // int pageNumber = Integer.parseInt(page.toString());
            String currentDateTime = new SimpleDateFormat("yyyy-MM-d k:m:s")
                    .format(new Date());
            String tillDate = "";
            String city = "";

            //TODO: change pagenumber somehow

            try {

                final String BASE_URL = "http://192.168.8.104/laravel-5-events/public/api/events?";
                final String FROM = "from";
                final String TILL = "till";
                final String CITY = "city";
                final String PAGE = "page";
                //final String PAGE_NUMBER = "1";

                Uri uriBuilder = Uri.parse(BASE_URL).buildUpon()
                        .appendQueryParameter(FROM, currentDateTime)
                        .appendQueryParameter(TILL, tillDate)
                        .appendQueryParameter(CITY, city)
                        .appendQueryParameter(PAGE, page.toString())
                        .build();

                URL url = new URL(uriBuilder.toString());
                Log.d(LOG, String.valueOf(url));

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuilder stringBuilder = new StringBuilder();
                // if (inputStream == null) return "inputStream == null";

                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line + "\n");
                }

                //if (stringBuilder.length() == 0) return "stringBuffer.length() == 0";

                eventString = stringBuilder.toString();
                getEventsFromJson(eventString);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
                if (bufferedReader != null) try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } //doInBackground

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.i(LOG, "onPostExecute");

            Cursor cursor = EventsDatabaseHelper.getInstance(mContext).read(mContext);
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(EventsDatabaseHelper.EVENT_NAME));
                String date = cursor.getString(cursor.getColumnIndex(EventsDatabaseHelper.START_DATE));
                System.out.println("EVENT NAME: " + name);
                System.out.println("EVENT DATE: " + date);
            }
            cursor.close();
        }
    } //class JsonTask

    private void getEventsFromJson(String eventsJsonString) throws JSONException {
        Log.i(LOG, "getEventsFromJson()");

        // Main node
        final String DATA = "data";

        // Id's
        final String USER_ID = "user_id";
        final String VENUE_ID = "venue_id";

        // Event
        final String EVENT_NAME = "event_name";
        final String START_DATE = "start_date";
        final String END_DATE = "end_date";
        final String EVENT_IMAGE = "event_image";
        final String PRICE = "price";
        final String EVENT_OTHER_DETAILS = "event_other_details";

        // Venue
        final String VENUE = "venue";
        final String VENUE_NAME = "venue_name";
        final String LAT = "lat";
        final String LNG = "lng";
        final String STREET_NUMBER = "street_number";
        final String VENUE_CITY = "city";
        final String ADDRESS = "address";
        final String COUNTRY = "country";
        final String VENUE_IMAGE = "venue_image";
        final String CAPABILITY = "capability";
        final String VENUE_OTHER_DETAILS = "venue_other_details";

        JSONObject eventsJsonObject = new JSONObject(eventsJsonString);
        JSONArray jsonArray = eventsJsonObject.getJSONArray(DATA);

        // Id's
        int userId;
        int venueId;

        // Event
        String eventName;
        String startDate;
        String endDate;
        String eventImage;
        String price;
        String eventOtherDetails;

        // Venue
        int venueUserId;
        String venueName;
        double lat;
        double lng;
        String streetNumber;
        String city;
        String address;
        String country;
        String venueImage;
        int capability;
        String venueOtherDetails;

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject arrayItem = jsonArray.getJSONObject(i);

            // Id's
            userId = arrayItem.getInt(USER_ID);
            venueId = arrayItem.getInt(VENUE_ID);

            // Event
            eventName = arrayItem.getString(EVENT_NAME);
            startDate = arrayItem.getString(START_DATE);
            endDate = arrayItem.getString(END_DATE);
            eventImage = arrayItem.getString(EVENT_IMAGE);
            price = arrayItem.getString(PRICE);
            eventOtherDetails = arrayItem.getString(EVENT_OTHER_DETAILS);

            ContentValues contentValues = new ContentValues();
            contentValues.put(EventsDatabaseHelper.EVENT_NAME, eventName);
            contentValues.put(EventsDatabaseHelper.START_DATE, startDate);
            contentValues.put(EventsDatabaseHelper.END_DATE, endDate);
            contentValues.put(EventsDatabaseHelper.EVENT_IMAGE, eventImage);
            contentValues.put(EventsDatabaseHelper.PRICE, price);
            contentValues.put(EventsDatabaseHelper.EVENT_OTHER_DETAILS, eventOtherDetails);

            EventsDatabaseHelper.getInstance(mContext).save(mContext, contentValues, EventsDatabaseHelper.EVENT_TABLE);
            contentValues = null;

            // Venue
            JSONObject venueJsonObject = arrayItem.getJSONObject(VENUE);

            venueUserId = venueJsonObject.getInt(USER_ID);
            venueName = venueJsonObject.getString(VENUE_NAME);
            lat = venueJsonObject.getDouble(LAT);
            lng = venueJsonObject.getDouble(LNG);
            streetNumber = venueJsonObject.getString(STREET_NUMBER);
            city = venueJsonObject.getString(VENUE_CITY);
            address = venueJsonObject.getString(ADDRESS);
            country = venueJsonObject.getString(COUNTRY);
            venueImage = venueJsonObject.getString(VENUE_IMAGE);
            capability = venueJsonObject.getInt(CAPABILITY);
            venueOtherDetails = venueJsonObject.getString(VENUE_OTHER_DETAILS);

            contentValues = new ContentValues();
            contentValues.put(EventsDatabaseHelper.EVENT_ID, EventsDatabaseHelper.rowId);
            contentValues.put(EventsDatabaseHelper.VENUE_NAME, venueName);
            contentValues.put(EventsDatabaseHelper.LAT, lat);
            contentValues.put(EventsDatabaseHelper.LNG, lng);
            contentValues.put(EventsDatabaseHelper.STREET_NUMBER, streetNumber);
            contentValues.put(EventsDatabaseHelper.CITY, city);
            contentValues.put(EventsDatabaseHelper.ADDRESS, address);
            contentValues.put(EventsDatabaseHelper.COUNTRY, country);
            contentValues.put(EventsDatabaseHelper.VENUE_IMAGE, venueImage);
            contentValues.put(EventsDatabaseHelper.CAPABILITY, capability);
            contentValues.put(EventsDatabaseHelper.VENUE_OTHER_DETAILS, venueOtherDetails);

            EventsDatabaseHelper.getInstance(mContext).save(mContext, contentValues, EventsDatabaseHelper.VENUE_TABLE);
            contentValues = null;
        }

    }


} //class EventListFragment
